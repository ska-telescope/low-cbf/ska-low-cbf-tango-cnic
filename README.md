# Low.CBF CNIC Tango Device

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf-tango-cnic/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf-tango-cnic/en/latest/?badge=latest)

## Features
* Dynamic Tango Attribute creation for all `user_attributes` defined in the ICL software

## Limitations
* VirtualDigitiser mode only supports a single subarray at present

## Developer's Guide

* Underlying Low CBF repos:
  * [Low CBF CNIC Control Software](https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-sw-cnic)
  * [Low CBF FPGA Software Interface](https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fpga)
  * [Low CBF Firmware CNIC](https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-cnic)
* The Makefiles here are inherited from
  [ska-cicd-makefile](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile).
  * Refer to docs at that repo, or use `make help` for details.
  * This link is via a git submodule, so use the `--recursive` flag when cloning
this repository, or run `git submodule update --init --recursive` afterwards.
* A pre-commit config is provided to try and format your code before it gets
rejected by the CI pipeline lint checkers.
  * Install [pre-commit](https://pre-commit.com/) - `pip3 install pre-commit`
  * Activate the pre-commit checks for the repo - `pre-commit install`
  * From now on your commits will be formatted automatically. Note that you
have to add & commit again any files changed by pre-commit.
* A git hook is provided that may help comply with SKA commit message rules.
You can install the hook with `cp -s "$(pwd)/resources/git-hooks"/* .git/hooks`.
Once installed, the hook will insert commit messages to match the JIRA ticket
from the branch name.
e.g. On branch `perentie-1350-new-base-classes`:
```console
ska-low-cbf$ git commit -m "Add git hook note to README"
Branch perentie-1350-new-base-classes
Inserting PERENTIE-1350 prefix
[perentie-1350-new-base-classes 3886657] PERENTIE-1350 Add git hook note to README
 1 file changed, 7 insertions(+)
```
You can see the modified message above, and confirming via the git log:
```console
$ git log -n 1 --oneline
3886657 (HEAD -> perentie-1350-new-base-classes) PERENTIE-1350 Add git hook note to README
```

## Changelog

See [CHANGELOG.md](CHANGELOG.md)

## Copyright Note
Repository icon from [Tango icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/tango)
