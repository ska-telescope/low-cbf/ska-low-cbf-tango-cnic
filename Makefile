# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define the OCI tag for this project.
PROJECT = ska-low-cbf-tango-cnic
KUBE_NAMESPACE ?= $(PROJECT)
RELEASE_NAME ?= test

CI_JOB_ID ?= local##pipeline job id
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server

K8S_CHART ?= test-parent
K8S_CHARTS = $(K8S_CHART)
HELM_CHARTS_TO_PUBLISH = $(PROJECT)
HELM_CHARTS ?= $(HELM_CHARTS_TO_PUBLISH)
K8S_TEST_RUNNER = test-runner-$(CI_JOB_ID)##name of the pod running the k8s-test
MINIKUBE ?= true
ITANGO_ENABLED ?= false
HELM_VERSION = v3.3.1
KUBERNETES_VERSION = v1.19.2

include .make/base.mk
include .make/python.mk
include .make/k8s.mk
include .make/helm.mk
include .make/oci.mk

DOCS_SPHINXOPTS = -n -W --keep-going

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --no-root --only docs

.PHONY: docs-pre-build

PYTHON_LINE_LENGTH = 88

# Add the ability to specify a VALUES_FILE at runtime
ifneq ($(VALUES_FILE),)
	CUSTOM_VALUES := --values $(VALUES_FILE)
else
endif

ifneq ($(CI_REGISTRY),)
	K8S_TEST_TANGO_IMAGE = --set ska-low-cbf-tango-cnic.cnic.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
		--set ska-low-cbf-tango-cnic.cnic.image.registry=$(CI_REGISTRY)/ska-telescope/low-cbf/ska-low-cbf-tango-cnic
	K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/low-cbf/ska-low-cbf-tango-cnic/ska-low-cbf-tango-cnic:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
	K8S_TEST_TANGO_IMAGE = --set ska-low-cbf.cnic.image.tag=$(VERSION)
	K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-low-cbf-tango-cnic:$(VERSION)
endif

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.exposeAllDS=$(EXPOSE_All_DS) \
	--set global.tango_host=$(TANGO_HOST) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set global.device_server_port=$(TANGO_SERVER_PORT) \
	--set ska-tango-base.display=$(DISPLAY) \
	--set ska-tango-base.xauthority=$(XAUTHORITY) \
	--set ska-tango-base.jive.enabled=$(JIVE) \
	--set ska-tango-base.itango.enabled=$(ITANGO_ENABLED) \
	${K8S_TEST_TANGO_IMAGE} \
	${CUSTOM_VALUES}

# When incrementing version
# (via make bump-patch-release or bump-minor-release or bump-major-release)
# update SKA_LOW_CBF_TANGO_VERSION variable used for integration test CI job
post-set-release:
	@echo "Replacing SKA_LOW_CBF_TANGO_CNIC_VERSION variable in .gitlab-ci.yml for v$(VERSION)"
	@sed -i 's/SKA_LOW_CBF_TANGO_CNIC_VERSION:.*/SKA_LOW_CBF_TANGO_CNIC_VERSION: "$(VERSION)-dev.c$${CI_COMMIT_SHORT_SHA}"/' .gitlab-ci.yml
