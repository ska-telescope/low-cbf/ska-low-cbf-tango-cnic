# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Virtual Digitiser interfacing logic tests."""

from ska_low_cbf_sw_cnic import NULL_SOURCE, Source, StreamConfig, VDChannelConfig

from ska_low_cbf_tango_cnic.virtual_digitiser import (
    create_vd_stream_configs,
    get_delay_map,
)


def test_create_vd_stream_configs():
    """Test create_vd_stream_configs."""
    # Example spec:
    # X: Channel 140 amplitude 30, Channel 141 amplitude 60, Channel 142 amplitude 120
    # Y: Channel 140 amplitude 20, Channel 141 amplitude 40, Channel 142 amplitude 80
    # Station 345 frequency -50, Station 350 frequency 0, Station 352 frequency +50

    # Showing Channel 140 & 141 for Station 345 only:
    config = [  # config is a list of dicts - one per SPEAD stream
        {  # SPEAD stream 0
            "scan": 0,
            "subarray": 1,
            "station": 345,
            "substation": 1,
            "frequency": 140,
            "beam": 1,
            "sources": {
                # up to 4 sources for each polarisation x, y
                # if less than 4 specified (or a polarisation missing),
                # NULL_SOURCE assumed
                "x": [
                    {"tone": True, "fine_frequency": -50, "scale": 30},
                ],
                "y": [
                    {"tone": True, "fine_frequency": -50, "scale": 20},
                ],
            },
        },
        {  # SPEAD stream 1
            "scan": 0,
            "subarray": 1,
            "station": 345,
            "substation": 1,
            "frequency": 141,
            "beam": 1,
            "sources": {
                "x": [
                    {"tone": True, "fine_frequency": -50, "scale": 60},
                ],
                "y": [
                    {"tone": True, "fine_frequency": -50, "scale": 40},
                ],
            },
        },
    ]
    expected_result = [
        StreamConfig(
            VDChannelConfig(
                scan=0, beam=1, frequency=140, substation=1, subarray=1, station=345
            ),
            [
                Source(
                    channel_frequency=0.109375, fine_frequency=-50, scale=30, tone=True
                ),
                NULL_SOURCE,
                NULL_SOURCE,
                NULL_SOURCE,
                Source(
                    channel_frequency=0.109375, fine_frequency=-50, scale=20, tone=True
                ),
                NULL_SOURCE,
                NULL_SOURCE,
                NULL_SOURCE,
            ],
        ),
        StreamConfig(
            VDChannelConfig(
                scan=0, beam=1, frequency=141, substation=1, subarray=1, station=345
            ),
            [
                Source(
                    channel_frequency=0.11015625,
                    fine_frequency=-50,
                    scale=60,
                    tone=True,
                ),
                NULL_SOURCE,
                NULL_SOURCE,
                NULL_SOURCE,
                Source(
                    channel_frequency=0.11015625,
                    fine_frequency=-50,
                    scale=40,
                    tone=True,
                ),
                NULL_SOURCE,
                NULL_SOURCE,
                NULL_SOURCE,
            ],
        ),
    ]
    assert create_vd_stream_configs(config) == expected_result


def test_delay_map():
    """Test get_delay_map."""
    stream_configs = [
        StreamConfig(
            VDChannelConfig(
                subarray=3,
                beam=4,
                substation=5,
                frequency=66,
                station=7,
                scan=888,
            ),
            [
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
            ],
        ),
        StreamConfig(
            VDChannelConfig(
                subarray=3,
                beam=4,
                substation=9,
                frequency=66,
                station=9,
                scan=888,
            ),
            [
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
            ],
        ),
        StreamConfig(
            VDChannelConfig(
                subarray=3,
                beam=4,
                substation=9,
                frequency=67,
                station=9,
                scan=888,
            ),
            [
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
                Source(tone=True, channel_frequency=0.1, scale=128),
            ],
        ),
    ]
    expected_result = {
        "source_s03_b04_1": {(7, 5): [(0, 4)], (9, 9): [(8, 12), (16, 20)]},
        "source_s03_b04_2": {(7, 5): [(1, 5)], (9, 9): [(9, 13), (17, 21)]},
        "source_s03_b04_3": {(7, 5): [(2, 6)], (9, 9): [(10, 14), (18, 22)]},
        "source_s03_b04_4": {(7, 5): [(3, 7)], (9, 9): [(11, 15), (19, 23)]},
    }
    assert get_delay_map(stream_configs) == expected_result
