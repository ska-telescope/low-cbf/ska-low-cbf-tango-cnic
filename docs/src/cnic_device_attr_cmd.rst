CNIC device Tango attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automethod:: cnic_device::CnicDevice.activePersonality
   :noindex:
.. automethod:: cnic_device::CnicDevice.delayMap
   :noindex:
.. automethod:: cnic_device::CnicDevice.platform
   :noindex:
.. automethod:: cnic_device::CnicDevice.serialNumber
   :noindex:

CNIC device Tango commands
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automethod:: cnic_device::CnicDevice.CallMethod 
   :noindex:
.. automethod:: cnic_device::CnicDevice.ConfigurePulsarMode 
   :noindex:
.. automethod:: cnic_device::CnicDevice.ConfigureSourcesFromYAML 
   :noindex:
.. automethod:: cnic_device::CnicDevice.ConfigureVirtualDigitiser 
   :noindex:
.. automethod:: cnic_device::CnicDevice.SelectPersonality 
   :noindex:
.. automethod:: cnic_device::CnicDevice.StartRegisterLog 
   :noindex:
.. automethod:: cnic_device::CnicDevice.StartSourceDelays 
   :noindex:
.. automethod:: cnic_device::CnicDevice.StartTmSourceDelays 
   :noindex:
.. automethod:: cnic_device::CnicDevice.StopSourceDelays 
   :noindex:
