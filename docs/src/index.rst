Welcome to ska-low-cbf-tango-cnic's documentation!
==================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   api/index

Tango attribute/command list
############################
.. note:: All ``CNIC`` attributes/commands are considered "INTERNAL" i.e. for testing and debugging purposes.

.. include:: cnic_device_attr_cmd.rst
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
