# Version History
This list is in reverse-chronological order (the latest change is at the top).
Different heading levels are used for Major, Minor, and Patch releases - in Markdown
syntax, that's `#` for Major, `##` for Minor, and `###` for Patch.

### 0.5.3
* Update to ska-low-cbf-sw-cnic v0.14.0
  * Requires firmware v0.1.14+

### 0.5.2
* Update ska-low-cbf-fpga to v0.19.8 (for new CAR firmware download API)

### 0.5.1
* Update CNIC software to v0.12.1
  * Requires firmware v0.1.13+ (2 Nov 2024 build or newer) for all FPGA types, even if no
    VD present

## 0.5.0
* Add `platform` attribute, reports FPGA platform, e.g. "xilinx_u280_gen3x16_xdma_base_1"
* Add `fpga_type` argument to `SelectPersonality` command
* Update CNIC software to v0.11.2
  * Requires firmware v0.1.13+

### 0.4.4
* Cache FPGA downloads in pod by default (downstream users should use a shared persistent
location for best results)
* Implement handling of web server URL when dowlnloading CNIC firmware; usage example:

    cnic.SelectPersonality( '{ "url": "http://192.168.20.2/dev/cnic_fw.tar.gz" }' )

### 0.4.3
* Expand default shared HBM memory to 4 × 4095M (i.e. all HBM is shared) - can be reduced via
`SelectPersonality`
* Raise Helm chart resource limits to 4 CPUs and 32GiB ram
* Update to ska-low-cbf-fpga v0.19.5, ska-low-cbf-sw-cnic v0.11.1

### 0.4.2
* Update pytango to 9.5.0

### 0.4.1
* Handle delays with >1024 VD streams via `vd_datagen_2` (requires firmware v0.1.12+)

## 0.4.0
* Add `ptp_domain` device property. **Warning** this property defaults to domain 0, and
 overrides the default value of 24 in ska-low-cbf-sw-cnic.

### 0.3.1
* Update to ska-low-cbf-sw-cnic v0.10.1
* Remove re-calculation of first packet number (workaround for CNIC software bug)

## 0.3.0
* Breaking change: Update to new TelModel Delay Polynomial format (keyword changes). Requires use of matching updated delay-poly-generator
* Allow client to select SPS packet version (ConfigureVirtualDigitiser)
* Allow client to control all `CnicFpga.configure_vd` arguments via ConfigureVirtualDigitiser command
* Update ska-low-cbf-sw-cnic to v0.10.0
* Add support for more than 1024 channels (use `vd_datagen_2` FPGA peripheral)
* In Helm charts, rename Alveo resource name (in k8s cluster) from `xilinx.com/fpga-xilinx_u55c_gen3x16_xdma_base_3-0` to `amd.com/xilinx_u55c_gen3x16_xdma_base_3-0`

### 0.2.2
* Update ska-low-cbf-sw-cnic to v0.8.0
* Modify Helm chart for comatibility with upstream development version logic

### 0.2.1
* Point to correct image in Helm chart
* Fix mid-block delay adjustment

## 0.2.0
* **Warning** the 0.2.0 Helm chart incorrectly uses the 0.1.1 container image.
To use this chart, please override ska-low-cbf-tango-cnic.cnic.image.tag to 0.2.0
* Add `Limitations` section to `README.md` file to summarise such details in a single place
* Increased spacing between packets for CNIC VD, to avoid correlator failure from HBM congestion
  - The increased spacing now limits overall data to 30Gbps
* use `ConfigurePulsarMode()` command to enable/disable/configure Virtual Digitiser "pulsar" mode
* Allow `StopSourceDelays` to be called whether delays have been started or not
* Call `StopSourceDelays` behind the scenes when user calls `SelectPersonality`
* Added 16-sample time delay so VHDL delay calculation at start of each 32-sample block gets the right average delay
value for the block

### 0.1.1
* Update dependency on ska-low-cbf-sw-cnic and ska-low-cbf-fpga for image caching
* Enable caching option for fpga images when CACHE_DIR environment variable is set
* Note: VirtualDigitiser mode only supports a single subarray at present

## 0.1.0
* Add Virtual Digitiser Data Generator control
* Create initial Tango wrapper for CNIC software
* Bootstrap repo with ska-cookiecutter-pypackage
