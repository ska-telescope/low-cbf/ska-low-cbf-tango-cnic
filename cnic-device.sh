#!/usr/bin/env bash

# script to help the helm template for device servers.
# we have to give it a command to run the device server,
# the template will add the instance parameter

echo "Tango device launch script called with arguments: $@"
# set up Xilinx XRT driver
source /opt/xilinx/xrt/setup.sh

# run the device server with parameters (e.g. instance) we were given
cnic_device "$@" -v
