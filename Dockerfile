ARG BUILD_IMAGE="artefact.skao.int/ska-tango-images-pytango-builder:9.5.0"
ARG BASE_IMAGE="artefact.skao.int/ska-tango-images-pytango-runtime:9.5.0"

FROM $BUILD_IMAGE as buildenv
FROM $BASE_IMAGE

# TODO detect Ubuntu version instead?
ARG UBUNTU_VERSION="22.04"
ARG XRT_VERSION="202220.2.14.354"
ARG XRT_URL="https://www.xilinx.com/bin/public/openDownload?filename=xrt_${XRT_VERSION}_${UBUNTU_VERSION}-amd64-xrt.deb"

USER root

RUN echo XRT URL is: $XRT_URL \
    && apt update && apt install -y wget \
    && wget --no-verbose --output-document xrt.deb "${XRT_URL}" \
    && apt install -y ./xrt.deb \
    && rm -rf /var/lib/apt/lists/* xrt.deb


RUN poetry self update -n 1.8.2
RUN poetry config virtualenvs.create false
RUN poetry install

USER tango
