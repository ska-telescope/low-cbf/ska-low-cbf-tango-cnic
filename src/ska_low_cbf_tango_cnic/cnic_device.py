# -*- coding: utf-8 -*-
#
# (c) 2020 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE for more info.
#
# pylint: disable=attribute-defined-outside-init,logging-fstring-interpolation

"""ARGS FPGA Tango wrapper."""

import gc
import json
import logging
import os
import re
import threading
from time import time
from typing import Optional

import numpy as np
import tango
from ska_low_cbf_fpga import (
    ArgsWordType,
    FpgaHardwareInfo,
    FpgaPersonality,
    IclFpgaField,
    create_driver_map_info,
    log_to_file,
    mem_parse,
)
from ska_low_cbf_fpga.download import get_file_fetcher
from ska_low_cbf_sw_cnic import CnicFpga
from tango import AttrQuality, AttrWriteType
from tango.server import Device, attribute, command, device_property, run

from ska_low_cbf_tango_cnic.delay_handler import DelayHandler
from ska_low_cbf_tango_cnic.delay_subscription import DelayPolySubscriber
from ska_low_cbf_tango_cnic.virtual_digitiser import (
    create_vd_stream_configs,
    get_delay_map,
)

DEFAULT_XILINX_PLATFORM = 3
DEFAULT_MEMORY_CONFIG = "4095Ms:4095Ms:4095Ms:4095Ms"

# limits for pulsar mode
MIN_SAMPLE_COUNT = 16
MAX_SAMPLE_COUNT = 0xFFFF_FFFF  # underlying FPGA registers are 32-bit


def _split_icl_attr_name(name) -> (Optional[str], str):
    """
    Decode a dynamic FPGA attribute name into peripheral & field names.

    e.g.

    - "top_level" -> None, "top_level"
    - "peripheral_name__field_name" -> "peripheral_name", "field_name"

    :param name: dynamic attribute name
    :returns: Peripheral name (or None for top-level), attr name
    """
    double_underscores = name.count("__")
    if double_underscores == 0:
        # top level attr
        return None, name
    if double_underscores == 1:
        # peripheral attr
        return name.split("__")

    raise RuntimeError(f"Invalid FPGA attribute name {name}")


class CnicDevice(Device):
    """Low CBF CNIC Tango Device."""

    # pylint: disable=too-many-instance-attributes

    ptp_domain = device_property(
        dtype=int,
        doc="PTP Domain",
        default_value=0,
        update_db=True,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialise."""
        super().init_device()
        self.logger = logging.getLogger()
        # despite all efforts, only warning or higher log levels work
        self.logger.critical("Crit?")
        self.logger.error("Error?")
        self.logger.warning("Warning?")
        self.logger.info("Info?")
        self.logger.debug("Debug?")
        self._write_lock = threading.Lock()
        self._active_personality = None
        self._core: Optional[CnicFpga] = None
        _, _, hw_info = create_driver_map_info()
        self._hw_info: Optional[FpgaHardwareInfo] = hw_info
        self._icl_attributes = set()  # Attribute names created from ICL
        self._delay_map = {}  # used for delay polynomial updates to VD
        self._delay_poly_subs = {}
        """Delay Polynomial Subscribers.
        Key: attribute name (e.g. source_s01_b01_1)
        Value: DelayPolySubscriber instance"""
        self._delay_handler = None
        self._file_fetcher = None
        self.logger.warning("Init complete.")

    def delete_device(self):
        """Delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        self._drop_xrt_driver()
        self.set_state(tango.DevState.OFF)

    def _field_from_name(self, name) -> IclFpgaField:
        """
        Find the ``IclFpgaField`` object for a given attribute name.

        e.g.

        -  "top_level" -> self._core.top_level
        -  "peripheral_name__field_name" -> self._core.peripheral_name.field_name

        :return: The referenced ``IclFpgaField`` object
        """
        self._check_core()

        peripheral_name, field_name = _split_icl_attr_name(name)
        if peripheral_name:
            field = getattr(getattr(self._core, peripheral_name), field_name)
        else:
            field = getattr(self._core, field_name)

        return field

    def _read_fpga_attr(self, attr):
        """Update an FPGA attribute with its current value."""
        try:
            name = attr.get_name()
        except AttributeError as error:
            self.logger.error("Attribute error %s", error)
            return

        field = self._field_from_name(name)
        value = field.value
        if name != "vd_datagen__configuration":  # too big for k9s!
            self.logger.warning("Read attribute %s = %s", name, value)
        else:
            self.logger.warning("Read big attribute %s", name)
        attr.set_value(value)

    def _write_fpga_attr(self, attr):
        """Write a value to the FPGA."""
        try:
            name = attr.get_name()
        except AttributeError as error:
            self.logger.error("Attribute error %s", error)
            return
        with self._write_lock:
            val = attr.get_write_value()
            peripheral_name, attr_name = _split_icl_attr_name(name)
            if name != "vd_datagen__configuration":  # too big for k9s!
                self.logger.warning("Write attribute %s = %s", name, val)
            else:
                self.logger.warning("Write big attribute %s", name)

            if peripheral_name:
                setattr(getattr(self._core, peripheral_name), attr_name, val)
            else:
                setattr(self._core, attr_name, val)

    def __add_attr_from_icl_field(self, name, field):
        """Create a Tango attribute from an IclFpgaField object."""
        if field.user_write:
            access_mode = "read/write"
            access = AttrWriteType.READ_WRITE
        else:
            access_mode = "read only "
            access = AttrWriteType.READ

        self.logger.warning("Adding %s attribute %s", access_mode, name)
        # Oops - workaround for a subtle change to ArgsWordType
        # (didn't need this when ArgsWordType was an alias for np.uint32!)
        dtype = field.type_
        if dtype == ArgsWordType:
            dtype = np.uint32

        att = attribute(
            name=name,
            label=name.replace("__", "."),  # label as "peripheral.register"
            description=field.description,
            dtype=dtype,
            format=field.format,
            access=access,
        )
        try:
            self.add_attribute(
                att, r_meth=self._read_fpga_attr, w_meth=self._write_fpga_attr
            )
            self._icl_attributes.add(name)
            setattr(self, name, att)
        except tango.DevFailed as fail:
            self.logger.error("Failed to add %s (%s): %s", name, att, fail)

    def _create_fpga_attrs(self):
        """Create Tango attributes from ICL attributes."""
        if self._core is None:
            return
        # Top-level Personality attributes
        for user_attribute in self._core.user_attributes:
            field = getattr(self._core, user_attribute)
            # we use double underscore as peripheral/attr delimiter,
            # so we can't allow it in a name otherwise
            user_attribute = re.sub("__*", "_", user_attribute)
            self.__add_attr_from_icl_field(user_attribute, field)

        # Peripheral attributes
        for peripheral_name in self._core.peripherals:
            peripheral = getattr(self._core, peripheral_name)
            for user_attribute in peripheral.user_attributes:
                peripheral_name = re.sub("__*", "_", peripheral_name)
                user_attribute = re.sub("__*", "_", user_attribute)
                name = f"{peripheral_name}__{user_attribute}"
                field = getattr(peripheral, user_attribute)
                self.__add_attr_from_icl_field(name, field)

    def _delete_fpga_attrs(self):
        """Delete all FPGA ICL attributes."""
        for name in self._icl_attributes:
            self.remove_attribute(name)
        self._icl_attributes = set()

    # ------------------
    # Attributes methods
    # ------------------
    @attribute(dtype=str)
    def activePersonality(self) -> str:  # pylint: disable=invalid-name
        """
        Read the currently active personality (firmware).

        :return: ``None`` if none loaded otherwise personality name
        """
        if self._active_personality is None:
            return "", time(), AttrQuality.ATTR_INVALID
        return str(self._active_personality)

    @attribute(dtype=str)
    def platform(self):
        """Read the FPGA platform."""
        if self._hw_info is None:
            return "", time(), AttrQuality.ATTR_INVALID
        return self._hw_info["platform"]["static_region"]["vbnv"]

    @attribute(dtype=str)
    def serialNumber(self) -> str:  # pylint: disable=invalid-name
        """Read the FPGA serial number."""
        if self._hw_info is None:
            return "", time(), AttrQuality.ATTR_INVALID
        card_mgmt = self._hw_info["platform"]["controller"]["card_mgmt_controller"]
        return card_mgmt["serial_number"]

    @attribute(dtype=str)
    def delayMap(self) -> str:  # pylint: disable=invalid-name
        """Read the delay attribute map (for debugging)."""
        # can't use JSON here because it won't accept tuples as dict keys
        return str(self._delay_map)

    # --------
    # Commands
    # --------
    @command(
        dtype_in=str,
        doc_in="ICL method call spec in JSON",
        dtype_out=str,
        doc_out="Returned value (as str)",
    )
    def CallMethod(self, request) -> str:  # pylint: disable=invalid-name
        """
        Call an ICL method.

        :param request: JSON string, containing at least a method to call.
          To call a top level (``FpgaPersonality``) method with no arguments:

          .. code:: python

            {"method": "reset"}

          To call a peripheral method with arguments:

          .. code:: python

            {"peripheral": "adder", "method": "sum", "arguments": {"x": 1, "y": 2}}

        :return: method return value, converted to string
        """
        self.logger.warning("CallMethod(%s)", request)
        request = json.loads(request)
        return str(self._call_method(**request))

    def _call_method(
        self,
        method: str,
        peripheral: Optional[str] = None,
        arguments: Optional[str] = None,
    ):
        self._check_core()
        if arguments is None:
            arguments = {}

        fpga = self._core
        class_name = fpga.__class__.__name__
        if peripheral:  # get the peripheral's method ...
            if peripheral not in fpga.peripherals:
                raise RuntimeError(f"{class_name} has no {peripheral} peripheral")
            function_ = getattr(getattr(fpga, peripheral), method)
            function_name = f"{class_name}.{peripheral}.{method}"
        else:  # ... otherwise pick the top level method
            function_ = getattr(fpga, method, None)
            function_name = f"{class_name}.{method}"

        self.logger.warning(f"Invoking {function_name}({arguments})")
        return function_(**arguments)

    @command(dtype_in=str, doc_in="Personality name & version (JSON)")
    def SelectPersonality(self, request):  # pylint: disable=invalid-name
        """
        Select a new personality to download & program FPGA.

        :param request: Personality name & version (JSON string). e.g.

          .. code:: python

            {
                "personality": "cnic",
                "version": "0.1.5",
                "source": "gitlab",
                "platform": 3,
                "memory": "2Gs:2Gs",
                "fpga_type": "u55c",
                "url":    "http://192.168.20.2:8080:/dev/cnic_fw.tar.gz"
            }

        All keys are optional, for defaults see
        :py:meth:`ska_low_cbf_tango_cnic.cnic_device.CnicDevice._select_personality`
        """
        self.StopSourceDelays()  # delay subscription thread would block FW change
        self._active_personality = None
        self._delete_fpga_attrs()
        self._drop_xrt_driver()
        if request == "":  # Accept empty string for convenience
            request = {}
        else:
            request = json.loads(request)
        # TODO - launch a thread
        self._select_personality(**request)

    # pylint: disable-next=too-many-arguments
    def _select_personality(
        self,
        personality: str = "cnic",
        version: str = "",  # default to latest
        source: str = "nexus",  # default to Central Artefact Repository
        platform: int = DEFAULT_XILINX_PLATFORM,
        memory: str = DEFAULT_MEMORY_CONFIG,
        fpga_type: str = "u55c",
        url: str = "",
        # just in case the JSON sent to the command contains other keys
        **kwargs,  # pylint: disable=unused-argument
    ):
        """
        Download the specified personality.

        :param personality: Name of personality (probably <= 4 characters)
        :param version: desired version (the latest build if version is empty)
        :param source: the source name: one of 'nexus', 'gitlab'
        :param url: fetch FW from a web server using the given URL
        """
        self.logger.warning(
            f"{self.__class__.__name__} got a request to download "
            f"personality {personality}, version {version}, source {source}, "
            f"fpga type {fpga_type}, url {url}"
        )
        cache_dir = os.getenv("CACHE_DIR", default="")
        with self._write_lock:
            personality_class = FpgaPersonality
            if personality == "cnic":
                personality_class = CnicFpga
            if url:
                # source is used to determine the FileFetcher type
                # version is used to store URL
                version = source = url
            ffetcher = get_file_fetcher(source)
            self._file_fetcher = ffetcher(
                personality,
                version,
                platform_version=platform,
                cache_dir=cache_dir,
                fpga_type=fpga_type,
            )
            self.logger.warning(f"Downloaded {self._file_fetcher.image_file_path}")
            self._create_core(personality_class, memory)
        self._active_personality = personality

    def _create_core(self, personality_class: type[FpgaPersonality], memory: str):
        """
        Create our _core FpgaPersonality object.

        :param personality_class: Derived type of FpgaPersonality to use.
        :param memory: memory configuration string (see mem_parse).
        """
        fpgamap_path = getattr(self._file_fetcher, "map_file_path", None)
        xcl_file = getattr(self._file_fetcher, "image_file_path", None)
        mem_config = mem_parse(memory)
        # TODO - check if this is a problem when we're avoiding base classes :)
        # ska-low-cbf-fpga v0.17.2 introduced hierarchical loggers, which upset
        # Tango. So we break the chain here. (Maybe hierarchy was a mistake?)
        # fpga_logger = logging.getLogger("FPGA")
        fpga_logger = self.logger
        driver, args_map, self._hw_info = create_driver_map_info(
            fpgamap_path=fpgamap_path,
            mem_config=mem_config,
            xcl_file=xcl_file,
            logger=fpga_logger,
        )

        if args_map is not None and driver is not None:
            self._core = personality_class(
                driver,
                args_map,
                hardware_info=self._hw_info,
                logger=fpga_logger,
                ptp_domain=self.ptp_domain,
            )
            self.logger.warning(
                f"Created a {self._core.__class__.__name__} core, "
                f"using {driver.__class__.__name__} driver"
            )
            self._create_fpga_attrs()
        else:
            txt = f"mem={mem_config}, xcl={xcl_file}, fpgamap={fpgamap_path}"
            self.logger.error(txt)
            self.logger.error("Failed to create core!")

    def _drop_xrt_driver(self):
        """
        Release XRT driver object(s), as they may hold an Alveo card lock.

        Normally called before switching FPGA personality.
        """
        if self._core is not None:
            del self._core
            self._core = None
        if self._hw_info is not None:
            del self._hw_info
            self._hw_info = None
        gc.collect()

    @command(
        dtype_in="DevString",
        doc_in="File Name",
    )
    def StartRegisterLog(self, filename):  # pylint: disable=invalid-name
        """
        Start FPGA register transaction logging.

        :param filename: output file name
        :raises RuntimeError: if no FPGA core active
        """
        self._check_core()
        log_to_file(self._core.driver.logger, filename)

    def is_ConfigureSourcesFromYAML_allowed(self):  # pylint: disable=invalid-name
        """Experiment."""
        return self._core is not None

    @command(dtype_in=str, doc_in="YAML")
    def ConfigureSourcesFromYAML(self, sources):  # pylint: disable=invalid-name
        """
        Configure VD Data Generator sources (for debugging use!).

        :param sources: YAML string

        see also :py:meth:`cnic_fpga.CnicFpga.configure_from_yaml`
        """
        self._check_core()
        self._core.configure_from_yaml(sources)

    @command(dtype_in=str, doc_in="JSON structure containing stream configurations.")
    def ConfigureVirtualDigitiser(self, config):  # pylint: disable=invalid-name
        """
        Configure the Virtual Digitiser.

        :param config: JSON (string) structure along the lines of:

        .. code:: python

            {
                "sps_packet_version": 3,
                "stream_configs":  [  # a list of dicts - one per SPEAD stream
                    {
                         "scan":       123_1,
                         "subarray":   1,
                         "station":    345,
                         "substation": 1,
                         "frequency": 66,
                         "beam": 16,
                         "sources": {
                            "x": [
                                {"tone": True, "fine_frequency": 16, "scale": 8000},
                            ],
                            "y": [
                                {"tone": True, "fine_frequency": 16, "scale": 8000},
                            ]
                    },
                    # ...
                    ]
            }

        JSON dictionary keys are the method arguments defined in
        :py:meth:`cnic_fpga.CnicFpga.configure_vd`

        Input also accepts the old format (a subset), which is only the above
        ``stream_configs`` value (as a list, not a dictionary).
        """
        self._check_core()
        config = json.loads(config)
        if isinstance(config, list):
            # old format, for backward compatibility
            config = {"stream_configs": config}
        if not isinstance(config, dict):
            raise ValueError("invalid format")  # shouldn't get here
        # We need stream configs, other parameters optional
        # (defaults supplied in CNIC SW)
        if "stream_configs" not in config:
            raise KeyError("Missing stream_configs key")
        stream_configs = create_vd_stream_configs(config["stream_configs"])
        config["stream_configs"] = stream_configs
        self._delay_map = get_delay_map(stream_configs)
        if "sps_packet_version" in config:
            self.logger.warning(
                f"Using SPS packet version {config['sps_packet_version']}"
            )
        self._core.configure_vd(**config)

    @command(dtype_in=str, doc_in="FQDN of delay emulator device")
    def StartSourceDelays(self, fqdn):  # pylint: disable=invalid-name
        """
        Subscribe to delay polynomials & begin applying delays for our VD data streams.

        CNIC will start emitting VD data packets after delay polynomial  updates
        are received.

        The delay source device is assumed to have attributes like
        ``source_s{subarray:02d}_b{beam:02d}_{source}``
        (each subarray beam has 4 sources)
        """
        # TODO - handle <= 4 sources?
        # e.g. sometimes there is only one: source_s01_b01_1
        if self._delay_poly_subs:
            raise RuntimeError("Already subscribed. Use StopSourceDelays first.")
        self._delay_poly_subs = {
            attr: DelayPolySubscriber(self.logger) for attr in self._delay_map
        }
        for attr, subscriber in self._delay_poly_subs.items():
            subscriber.subscribe(fqdn, attr)

        self._delay_handler = DelayHandler(
            self._core,
            self._delay_map,
            self._delay_poly_subs,
            self._core.vd.configured_channels.value,
            logger=self.logger,
        )

    @command(dtype_in=str, doc_in="JSON dict of Delay URIs for each subarray/beam")
    def StartTmSourceDelays(self, subarray_bm_uris):  # pylint: disable=invalid-name
        """
        Command allows use of TM delay polynomials.

        Actions are:

        1. Subscribe to TM delay polynomials that describe relative delays of the
        signals received at each station from sky sources. (The delays are a function
        of the real-time direction of each sky source - directions of sources are
        specified as parameters to the delay-polynomial device, outside of CNIC.)
        As a simplification, in this mode of operation, there is only one direction
        (one polynomial source) for all 4 sources that can be generated for a beam.

        2. Apply delays to the data streams produced by the CNIC, so that simulated
        sky data is output as SKA SPEAD packet streams.

        CNIC will begin sending data packets when delay updates begin to be received.

        The ``ConfigureVirtualDigitiser`` command should have been issued before this
        one. Its parameters set the subarrays, stations, and substations for which the
        CNIC is to generate data. The arguments to ``StartTmSourceDelays`` must include
        an entry for every subarray and beam in ``ConfigureVirtualDigitiser``
        parameters.

        :param subarray_bm_uris: JSON dictionary containing a Tango URI for
          each station beam delay source. e.g. if subarray 3 has station beams 7 & 12
          and subarray 6 has station beam 4, we expect the JSON version of:

          .. code-block:: python

            {
                3: {7: ["uri_1", "attr_1"], 12: ["uri_2", "attr_2"]},
                6: {4: ["uri_3", "attr_3"]},
            }

        """
        # TODO - handle <= 4 sources? (Same issue as in StartSourceDelays)
        if self._delay_poly_subs:
            raise RuntimeError("Already subscribed. Use StopSourceDelays first.")

        # create a delay subscription for each subarray beam
        # subarray_beam_src_id format: "source_s{subarray:02d}_b{beam:02d}_{source}"
        self._delay_poly_subs = {
            subarray_beam_src_id: DelayPolySubscriber(self.logger)
            for subarray_beam_src_id in self._delay_map
        }

        # check argument has all required URIs. Each source (set by )
        # note: integer dict keys from JSON will be strings not ints, so indexing into
        #       "delay_uris" variable needs to use key values that are strings
        delay_uris = json.loads(subarray_bm_uris)
        assert isinstance(delay_uris, dict)
        for subarray_beam_src_id, subscriber in self._delay_poly_subs.items():
            subarray_txt = str(int(subarray_beam_src_id[8:10]))
            beam_txt = str(int(subarray_beam_src_id[12:14]))
            assert (
                subarray_txt in delay_uris
            ), f"Missing delay source for subarray {subarray_txt}"
            assert (
                beam_txt in delay_uris[subarray_txt]
            ), f"Missing delay source for beam {beam_txt}"

        # create delay subscriptions for each station beam
        for subarray_beam_src_id, subscriber in self._delay_poly_subs.items():
            # pick subarray_id and beam_id out of the attr name
            subarray_txt = str(int(subarray_beam_src_id[8:10]))
            beam_txt = str(int(subarray_beam_src_id[12:14]))
            dly_dev, dly_attr = delay_uris[subarray_txt][beam_txt]
            subscriber.subscribe(dly_dev, dly_attr)

        self._delay_handler = DelayHandler(
            self._core,
            self._delay_map,
            self._delay_poly_subs,
            self._core.vd.configured_channels.value,
            logger=self.logger,
        )

    @command
    def StopSourceDelays(self):  # pylint: disable=invalid-name
        """Unsubscribe from the source delay generator & cease applying delays."""
        for subscriber in self._delay_poly_subs.values():
            subscriber.unsubscribe()
        self._delay_poly_subs = {}

        if self._delay_handler:
            self._delay_handler.stop()
        self._delay_handler = None

    @command(dtype_in=str, doc_in="JSON string")
    def ConfigurePulsarMode(self, config_str):  # pylint: disable=invalid-name
        """Configure the Virtual Digitiser pulsar mode.

        :param config_str: Pulsar parameters as JSON string, e.g.

        .. code:: python

            { "enable": true, "sample_count": [ 32, 256, 32] }

        where ``sample_count`` entries (all >= 16) are, in order:

        - number of samples before start
        - number of samples during ON period
        - number of samples during OFF period

        see also
        `PERENTIE-2106 <https://jira.skatelescope.org/browse/PERENTIE-2106>`_

        ``sample_count`` is optional, if omitted the old values remain active
        """
        self._check_core()
        config = json.loads(config_str)
        assert "enable" in config
        enable = config["enable"]
        sample_counts = ()
        if "sample_count" in config:
            sample_counts = config["sample_count"]
            assert len(sample_counts) == 3
            for count in sample_counts:
                assert MIN_SAMPLE_COUNT <= count <= MAX_SAMPLE_COUNT
        self._core.vd.configure_pulsar_mode(enable, sample_counts)

    def _check_core(self):
        """Throw exception if FPGA core is missing otherwise just proceed."""
        if self._core is None:
            raise RuntimeError("No FPGA core active")


def main(args=None, **kwargs):
    """Launch device server."""
    return run((CnicDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
