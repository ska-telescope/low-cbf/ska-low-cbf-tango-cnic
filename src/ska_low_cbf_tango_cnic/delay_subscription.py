# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""Classes that subscribe to delay polynomial sources."""

import json
import logging
import random
from abc import ABC
from enum import IntEnum
from queue import Queue
from threading import Lock, Thread, Timer
from typing import Optional

import tango
from tango import DevFailed, DeviceProxy, EventData, EventSystemFailed

from ska_low_cbf_tango_cnic.delay_handler import DelayPolyInfo

NS_PER_PACKET = 2048 * 1080  # 2048 samples at 1080 ns per sample in packet
SECONDS_TO_ALIGN = 864  # every 864 secs, packets align with seconds boundary
PACKETS_TO_ALIGN = 390_625  # every 390625 pkts align with seconds boundary


class Evt(IntEnum):
    """Events relevant to delay subscription states."""

    ON_ENTRY = 0
    ON_EXIT = 1
    SUBSCRIBE = 2
    TIMEOUT = 3
    UNSUBSCRIBE = 4
    SUBSCRIBE_FAILURE = 5


class EvtHndlr(ABC):
    """Event handling logic."""

    def __init__(self, logger: logging.Logger):
        """
        Create an Event Handler.

        :param logger: for processing log messages.
        """
        self._cur_state = None
        self._evt_queue = Queue()
        self._logger = logger
        self._lock = Lock()
        self._is_processing = False

    def initial_state(self, initial_state):
        """Set starting state for handler."""
        self.transition(initial_state)
        self._process_evts()

    def transition(self, next_state):
        """Transition from one event handling state to another."""
        if self._cur_state is not None:
            self._cur_state(Evt.ON_EXIT, None)
        self._cur_state = next_state
        self._cur_state(Evt.ON_ENTRY, None)

    def add_evt(self, evt, evt_data):
        """Add an event to the queue of events to be processed."""
        self._evt_queue.put((evt, evt_data))
        # Ensure only one thread of execution processes events
        with self._lock:
            if self._is_processing:
                return
        self._process_evts()

    def _process_evts(self):
        """Process all events in queue until queue is empty."""
        # pylint: disable=consider-using-with
        self._lock.acquire()
        self._is_processing = True
        while not self._evt_queue.qsize() == 0:
            self._lock.release()

            # get next event & run current state's handling for the event
            evt, evt_data = self._evt_queue.get()
            self._cur_state(evt, evt_data)

            self._lock.acquire()
        self._is_processing = False
        self._lock.release()


class DelayPolySubscriber(EvtHndlr):
    """Class handling subscriptions to delay polynomial devices."""

    MAX_QUEUED_POLYS = 10

    def __init__(self, logger):
        """
        Create a Delay Polynomial Subscriber.

        :param logger: for processing log messages.
        """
        super().__init__(logger)

        self._dev_name = None  # Name of tango device we subscribe to
        self._tango_proxy = None  # proxy for device we subscribe to
        self._attr_name = None  # Name of attribute we subscribe to
        self._subscription_id = None  # id of successful subscription
        self._timer = None  # for retry of failed subscription
        self._delay_poly_q = Queue()  # Queue of delay polynomials
        self._on_new_poly_cbk = None  # callback when new poly received

        # Start in "idle state"
        self.initial_state(self._state_idle)
        logger.info("DelayPolySubscriber init complete")

    # Methods to handle events for each state

    def _state_idle(self, evt: Evt, evt_data):
        """Handle events in idle state."""
        if evt == Evt.SUBSCRIBE:
            self._dev_name, self._attr_name = evt_data
            self._do_try_subscribe()
            return
        # ON_ENTRY - nothing to do
        # ON_EXIT - nothing to do
        # TIMEOUT (impossible) - ignore
        # SUBSCRIBE_FAILURE - not possible in idle
        # UNSUBSCRIBE - ignore

    def _state_retry_wait(self, evt: Evt, _):
        """Retry event subscription."""
        if evt == Evt.ON_ENTRY:
            # start timer thread
            retry_secs = 10.0 + 2.0 * random.random()
            self._timer = Timer(retry_secs, self._do_retry)
            self._timer.start()
            return
        if evt == Evt.TIMEOUT:
            del self._timer
            self._timer = None
            self._do_try_subscribe()
            return
        if evt == Evt.UNSUBSCRIBE:
            self._timer.cancel()
            self._timer = None
            self.transition(self._state_idle)
            return
        # ON_EXIT - nothing to do
        # SUBSCRIBE_FAILURE - not possible in retry_wait
        # SUBSCRIBE - ignore

    def _state_subscribed(self, evt: Evt, _):
        """Handle Events received when successfully subscribed."""
        if evt == Evt.SUBSCRIBE_FAILURE:
            self.transition(self._state_retry_wait)
            return
        if evt == Evt.UNSUBSCRIBE:
            self.transition(self._state_idle)
            return
        if evt == Evt.ON_EXIT:
            self._logger.warning(
                "Unsubscribing %s, id=%s",
                self._attr_name,
                self._subscription_id,
            )
            self._tango_proxy.unsubscribe_event(self._subscription_id)
            self._logger.info("After unsubscribe")
            self._subscription_id = None
            del self._tango_proxy
            self._tango_proxy = None
            return
        # ON_ENTRY - nothing to do
        # SUBSCRIBE - ignore
        # TIMEOUT - ignore

    def _do_try_subscribe(self):
        """Attempt to subscribe to delay polynomial."""
        self._logger.warning(
            "Try subscribing to %s/%s ...", self._dev_name, self._attr_name
        )
        try:
            # Attempt to get device proxy for subscription target
            self._tango_proxy = DeviceProxy(self._dev_name)
        except DevFailed:
            self._logger.warning("Subscription fail: no Tango device)")
            self.transition(self._state_retry_wait)
            return

        try:
            # Attempt to subscribe to attribute
            self._subscription_id = self._tango_proxy.subscribe_event(
                self._attr_name,
                tango.EventType.CHANGE_EVENT,
                self._tango_event_receiver,
                stateless=True,
            )
        except EventSystemFailed:
            # Tango docs say this never occurs if stateless=True
            del self._tango_proxy
            self._tango_proxy = None
            self._logger.warning("Subscription fail: Tango evt sys fail")
            self.transition(self._state_retry_wait)
            return

        # Successful subscription
        self._logger.warning("Subscription success: id %s", self._subscription_id)
        self.transition(self._state_subscribed)

    def _do_retry(self):
        """
        Retry subscribing to a delay polynomial attribute.

        Called on timeout in the retry state, for another attempt.
        """
        self.add_evt(Evt.TIMEOUT, None)

    def _tango_event_receiver(self, tango_data: EventData):
        """Process a tango delay-poly attribute update."""
        if tango_data.err:
            for err in tango_data.errors:
                self._logger.warning("%s %s", tango_data.device, err.desc)
            self.add_evt(Evt.SUBSCRIBE_FAILURE, None)
            return

        # delay attribute value should be valid JSON
        try:
            value = json.loads(tango_data.attr_value.value)
        except json.JSONDecodeError:
            self._logger.warning("Discarded non-JSON delay polynomial")
            return
        # self._logger.info("Delay update from %s", tango_data.attr_name)
        self._delay_poly_q.put(value)

        # Limit number of polynomials queued
        while self._delay_poly_q.qsize() > self.MAX_QUEUED_POLYS:
            _ = self._delay_poly_q.get()

        # Notify new poly received
        if self._on_new_poly_cbk is not None:
            cbk_thread = Thread(target=self._on_new_poly_cbk, args=())
            cbk_thread.start()

    # Interface to external callers

    def subscribe(self, tango_dev_name, tango_attr_name):
        """
        Subscribe to a tango delay device attribute.

        :param tango_dev_name: FQDN of device for subscription
        :param tango_attr_name: name of device attribute for subscription
        """
        self.add_evt(Evt.SUBSCRIBE, (tango_dev_name, tango_attr_name))

    def unsubscribe(self):
        """Unsubscribe from delay poly source (required for new subscription)."""
        self.add_evt(Evt.UNSUBSCRIBE, None)

    def get_next_poly(self, latest: bool = False) -> Optional[DelayPolyInfo]:
        """
        Get the next polynomial from queue.

        :param latest: if true, return latest received and discard others
        :return: Next delay polynomial info, or ``None``
        """
        if self._delay_poly_q.qsize() == 0:
            return None

        # discard earlier polys if we only want latest
        if latest:
            while self._delay_poly_q.qsize() > 1:
                _ = self._delay_poly_q.get()

        value = self._delay_poly_q.get()
        # self._logger.info("value = %s", value)
        epoch = value["start_validity_sec"]
        validity_secs = value["validity_period_sec"]
        cadence_secs = value["cadence_sec"]
        poly = value["station_beam_delays"]

        cycle = epoch // SECONDS_TO_ALIGN
        in_cycle_nsecs = (epoch - cycle * SECONDS_TO_ALIGN) * 1_000_000_000
        in_cycle_pkts = in_cycle_nsecs // NS_PER_PACKET
        ns_offset = in_cycle_nsecs - in_cycle_pkts * NS_PER_PACKET
        pkt_no = cycle * PACKETS_TO_ALIGN + in_cycle_pkts
        self._logger.info("Epoch %s: packet %s, ns_offset %s", epoch, pkt_no, ns_offset)

        return DelayPolyInfo(epoch, validity_secs, cadence_secs, poly)

    def on_new_poly_cbk(self, cbk):
        """Register function to be called when a new polynomial is received."""
        self._on_new_poly_cbk = cbk
