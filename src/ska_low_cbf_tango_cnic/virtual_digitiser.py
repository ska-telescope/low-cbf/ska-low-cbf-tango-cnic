# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Helper functions to connect Tango to Virtual Digitiser."""
import typing
from collections import defaultdict

from ska_low_cbf_sw_cnic import (
    SOURCES_PER_STREAM,
    Source,
    StreamConfig,
    VDChannelConfig,
    frequency_from_id,
    pad_source_list,
)


def create_vd_stream_configs(
    overall_config: typing.List[dict],
) -> typing.List[StreamConfig]:
    """
    Create config objects.

    .. code-block:: python

        config = [  # config is a list of dicts - one per SPEAD stream
            {  # SPEAD stream 0
                "scan": 0,
                "subarray": 1,
                "station": 345,
                "substation": 1,
                "frequency": 140,  # channel number, probably 64-448
                "beam": 1,
                "sources": {
                    # up to 4 sources for each polarisation x, y
                    # if less than 4 specified (or a polarisation missing),
                    # NULL_SOURCE assumed
                    "x": [
                        {"tone": True, "fine_frequency": -50, "scale": 30},
                    ],
                    "y": [
                        {"tone": True, "fine_frequency": -50, "scale": 20},
                    ]
                },
            },
            # ... continues with one dict per SPEAD stream
        ]

    :param overall_config: Overall configuration. See above.
    :returns: a StreamConfig per SPEAD stream.
    """
    vd_chan_cfg_fields = (
        "scan",
        "beam",
        "frequency",
        "substation",
        "subarray",
        "station",
    )
    polarisations = ("x", "y")

    stream_configs = []
    for stream_cfg in overall_config:
        vd_cfg = {
            key: value for key, value in stream_cfg.items() if key in vd_chan_cfg_fields
        }
        spead_stream = VDChannelConfig(**vd_cfg)
        sources = []
        # channel_frequency is probably not present in input configuration
        # vd_datagen uses GHz
        channel_frequency = frequency_from_id(stream_cfg["frequency"]) / 1e9
        for n, pol in enumerate(polarisations, start=1):
            try:
                for source in stream_cfg["sources"][pol]:
                    # allowing user to manually specify, just in case...
                    if "channel_frequency" not in source:
                        source["channel_frequency"] = channel_frequency
                    sources.append(Source(**source))
            except KeyError:
                # this polarisation not specified (or no sources specified at all)
                pass
            # ensure we have 4 entries per polarisation
            sources = pad_source_list(sources, 4 * n)

        stream_configs.append(StreamConfig(spead_stream=spead_stream, sources=sources))

    return stream_configs


def get_delay_map(stream_configs: typing.List[StreamConfig]):
    """
    Get a map of source delay attribute name to vd_datagen index numbers.

    We will subscribe to attributes: 'source_s{subarray:02d}_b{beam:02d}_{source:1d}'
    according to beam configuration, and need to link these to right source in the FPGA
    when it comes time to update the delay polynomials.

    :param stream_configs: the output of create_vd_stream_configs
    :returns: mapping from delay attribute name to dict, which has
        key: tuple (station, substation),
        value: list of tuples of vd_datagen indices (x,y)
        that delays from that attribute should be applied to. e.g.
        {"source_s01_b01_1": {(1, 1): [(0, 4), (16, 20)] (2, 1): [(8, 12), (24, 28)]} }
    """
    delay_map = defaultdict(lambda: defaultdict(list))
    sources_per_polarisation = SOURCES_PER_STREAM // 2
    for n_stream, stream_config in enumerate(stream_configs):
        first_source_index = n_stream * SOURCES_PER_STREAM
        subarray = stream_config.spead_stream.subarray
        beam = stream_config.spead_stream.beam
        station = stream_config.spead_stream.station
        substation = stream_config.spead_stream.substation
        for source in range(sources_per_polarisation):
            # attrs use 1-4 indexing for the source number
            attr_name = f"source_s{subarray:02d}_b{beam:02d}_{source + 1:1d}"
            x_pol = first_source_index + source
            y_pol = x_pol + sources_per_polarisation
            delay_map[attr_name][(station, substation)].append((x_pol, y_pol))

    return dict(delay_map)  # no need for default entries in returned value
