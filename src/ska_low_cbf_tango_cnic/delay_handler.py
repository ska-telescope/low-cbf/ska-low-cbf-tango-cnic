# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Handle updates to Virtual Digitiser source delay polynomials."""
import copy
import logging
import time
import typing
from dataclasses import dataclass
from threading import Lock, Timer

from ska_low_cbf_sw_cnic import (
    DELAY_POLY_COEFFS,
    SOURCES_PER_STREAM,
    CnicFpga,
    DataGenerator,
    VirtualDigitiser,
)


@dataclass
class DelayPolyInfo:
    """Structure holding delay polynomial info."""

    epoch: int  # time at which polynomials first becme valid
    validity_secs: float  # length of time polynomials are valid
    cadence_secs: float  # time between successive polynomials
    poly: list  # list of dicts containing polys, one per (sub)station


class DelayHandler:
    """Handle updates to Virtual Digitiser source delay polynomials."""

    # pylint: disable=too-many-instance-attributes

    SPS_SAMPLE_NS = 1080
    """nanoseconds per SPS sample (exact)"""
    PKT_NS = 2048 * SPS_SAMPLE_NS
    """ nanoseconds per SPS packet (2048 samples/packet) """
    CNIC_OFFSET_NS = 15.5 * SPS_SAMPLE_NS
    """
    CNIC calculates delay for every 32-sample block at the start of the block
    but to get the correct average delay the calculation should be at the
    middle of the block. To avoid changing firmware, add 15.5 sample time-delay
    """
    POSITIVE_OFFSET_NS = 500_000
    """
    CNIC FPGA can only make positive delays, so we add 500usec
    bias to ensure delay calc will always result in a positive number
    (60km Array dia -> +/-200usec delay even if ref stn is at edge of array)
    """

    def __init__(
        self,
        fpga: CnicFpga,
        delay_map: dict,
        delay_poly_subs: dict,
        n_vd_spead_streams: int,
        logger=None,
    ):  # pylint: disable=too-many-arguments
        """
        Initialise DelayHandler.

        :param fpga: CNIC FPGA FpgaPersonality object
        :param delay_map: Map from source delay Tango attribute to VD indices (x,y)
        :param delay_poly_subs: Map from source delay Tango attribute to subscriber
        :param n_vd_spead_streams: number of VD SPEAD streams in use
        :param logger: a logger will be created if none supplied
        """
        self.fpga = fpga
        self.vd_datagen: DataGenerator = fpga.vd_datagen
        self.vd: VirtualDigitiser = fpga.vd
        self.delay_poly_subs = delay_poly_subs
        self.delay_map = delay_map
        self.logger = logger or logging.getLogger()
        n_vd_sources = n_vd_spead_streams * SOURCES_PER_STREAM
        self.logger.warning(f"N_VD_SOURCES: {n_vd_sources}")
        self._initial_epoch = None
        """First epoch (activation time) received from delay source."""
        self._next_polys = [[0] * DELAY_POLY_COEFFS] * n_vd_sources  # TODO - numpy?
        """The next set of delay polynomials to be applied, one per source."""

        self._no_poly_timer = None
        """Timer that will expire if polynomials stop arriving"""

        self._most_recent_epoch = -1
        self._is_first_poly = True
        self._lock = Lock()  # guards access to self._latest_polys
        self._latest_polys = {}
        """Most recently received poly for each subscription"""
        for name, sub in self.delay_poly_subs.items():
            self._latest_polys[name] = None
            sub.on_new_poly_cbk(self._on_new_poly)

        # call once to get any already-queued polynomials
        self._on_new_poly()

    def _on_new_poly(self):
        """
        Update with latest polynomials, decide when ready to program into FPGA.

        This is called every time a delay polynomial subscription is updated.
        The indication that new polynomials have been received by all subscriptions
        and are ready to be programmed into the FPGA is that all subscription
        start-of-validity times are identical. IE this code depends on all delay polys
        having updates synchronised to within one update cycle. (The delay-poly-gen
        will always satisfy the assumption.)
        """
        with self._lock:
            # Check all subscriptions for a new delay polynomial
            for name, sub in self.delay_poly_subs.items():
                dp_info = sub.get_next_poly(latest=True)
                if dp_info is None:
                    continue
                self._latest_polys[name] = dp_info
                self._most_recent_epoch = max(self._most_recent_epoch, dp_info.epoch)

            # If not all polys are updated to same start-of-validity: return
            for dp_info in self._latest_polys.values():
                if (dp_info is None) or (dp_info.epoch != self._most_recent_epoch):
                    return

            # All polys have same cadence and validity: read from the first one
            a_poly = list(self._latest_polys.values())[0]
            cadence_secs = a_poly.cadence_secs
            validity_secs = a_poly.validity_secs

            # record time at which polynomial update begins
            starting_dg_secs = self.vd_datagen.read_time()
            starting_unix_t = time.time()

            # cancel timer because polynomial has arrived
            if self._no_poly_timer is not None:
                self._no_poly_timer.cancel()
                self._no_poly_timer = None

            # save the updated polys in self._next_polys
            for name, dp_info in self._latest_polys.items():
                self._save_delay_update(dp_info.poly, name)

            # Update FPGA with new polynomials
            if self._is_first_poly:
                # Special initialisation needed when starting sending
                self._is_first_poly = False
                self._initial_epoch = self._most_recent_epoch
                self._do_first_poly_update()
            else:
                self.logger.warning(
                    f"configure_next_delay_polynomials({len(self._next_polys)} polys, "
                    f"{self._most_recent_epoch})"
                )
                self.fpga.configure_next_delay_polynomials(
                    self._next_polys, int(self._most_recent_epoch)
                )

            # Log the timing parameters of this polynomial update
            earliest_valid = (
                self._most_recent_epoch - cadence_secs
            )  # Time of the previous poly's start validity
            latest_valid = self._most_recent_epoch + validity_secs  # this poly end
            ending_dg_secs = self.vd_datagen.read_time()
            txt = f"unix_t={starting_unix_t:.2f}"
            txt += f" datagen_t={starting_dg_secs:.2f} :"
            txt += f" PolysValid t=[{earliest_valid}:{latest_valid}]"
            txt += f"  [PolyUpdate in unix_t={time.time()-starting_unix_t:.2f}"
            txt += f" datagen_t={ending_dg_secs-starting_dg_secs:.2f} ]"
            if earliest_valid <= ending_dg_secs <= latest_valid:
                self.logger.warning(txt)
            else:
                txt += "  Error no valid polynomial"
                self.logger.error(txt)

            # Start a timer in case next polynomial doesn't arrive
            time_to_not_valid = latest_valid - ending_dg_secs
            self._no_poly_timer = Timer(
                time_to_not_valid, self._no_poly_timeout, args=()
            )
            self._no_poly_timer.start()

    def _do_first_poly_update(self):
        """Perform initialisation and first delay update."""
        # configure VD packet timestamp
        spead_epoch = self._initial_epoch
        spead_timestamp = 0
        sps_version = self.vd.sps_packet_version.value
        self.logger.warning(
            f"SPS protocol v{sps_version}. Setting VD SPEAD epoch to {spead_epoch}"
        )
        if sps_version >= 3:
            self.vd.configure_time(spead_epoch)  # float OK here
        else:
            self.vd.configure_time_v2(int(spead_epoch), spead_timestamp)

        # calculate datagen start time offsets
        first_pkt_no = self.vd.packet_count.value  # set in configure_time[_v2]
        data_start_sec = int(self._initial_epoch)
        data_start_ns = int(first_pkt_no * self.PKT_NS - (data_start_sec * 1e9))

        # Time delay: CNIC should eval delay at middle of block not start
        data_start_ns += int(self.CNIC_OFFSET_NS)
        if data_start_ns >= 1_000_000_000:
            data_start_ns -= 1_000_000_000
            data_start_sec += 1

        # Write delays for all sources at T=0 to FPGA
        datagen_use_time = int(self._initial_epoch)
        self.logger.warning(
            f"configure_next_delay_polynomials({len(self._next_polys)} polys, "
            f"{datagen_use_time}, {data_start_sec}, {data_start_ns})"
        )
        self.fpga.configure_next_delay_polynomials(
            self._next_polys, datagen_use_time, data_start_sec, data_start_ns
        )

        # reset DataGenerator back to T=0
        self.vd.reset_vd_data_gen_logic = 1
        self.vd.reset_vd_data_gen_logic = 0

        # start sending packets (and consuming delay polynomials)
        self.logger.warning("Enabling VD")
        self.fpga.enable_vd = True

    def _no_poly_timeout(self):
        """Log an error if timer expires due to no new polynomials."""
        self.logger.error("No valid polynomial. Previous polynomial expired.")

    @property
    def current_vd_time(self) -> typing.Optional[int]:
        """
        The current time, calculated from VD data generator + initial offset.

        :returns None: if initial offset hasn't been received.
        """
        if self._initial_epoch is None:
            return None

        return self._initial_epoch + self.vd_datagen.current_time_s.value

    def _save_delay_update(self, delay_details: dict, source_attribute: str) -> None:
        """
        Copy coefficients from a delay-polynomial update to "self._next_polys" buffer.

        (Buffer will be written to FPGA when coefficients for all attrs are ready.)

        :param delay_details: delay_details portion of attr value from delay emulator
        :param source_attribute: originating delay emulator attribute, e.g.
            'source_s01_b01_1'
        """
        # self.logger.warning(delay_details)
        station_sources = self.delay_map[source_attribute]
        # station_sources will look like {(1, 1): [(0, 4)], (2, 1): [(8, 12), (16, 20)]}
        # keys are (station, substation), values are VD datagen source indices (x,y)
        for (station, substation), source_indices in station_sources.items():
            poly_info = None
            # find correct station from the list of station delays
            for delay_detail in delay_details:
                if (
                    delay_detail["station_id"] == station
                    and delay_detail["substation_id"] == substation
                ):
                    # Delays must be globally offset - CNIC can't do negative delays
                    xpol_coeffs = copy.copy(delay_detail["xypol_coeffs_ns"])
                    xpol_coeffs[0] += self.POSITIVE_OFFSET_NS
                    ypol_coeffs = copy.copy(xpol_coeffs)
                    # Allow Y-pol delay to be offset from X-pol delay
                    ypol_coeffs[0] += delay_detail["ypol_offset_ns"]
                    # The final polys to be applied to X+Y pols of the station
                    poly_info = {"X": xpol_coeffs, "Y": ypol_coeffs}
                    break
            if poly_info is None:
                self.logger.error(
                    f"Didn't find our receptor! Station {station}.{substation}"
                )
                continue

            for x_index, y_index in source_indices:
                self._next_polys[x_index] = poly_info["X"]
                self._next_polys[y_index] = poly_info["Y"]
                # self.logger.warning(
                #    f"station.sub: {station}.{substation}, x: {x_index}, y: {y_index}"
                #    f" X[0]={self._next_polys[x_index][0]}"
                #    f" Y[0]={self._next_polys[y_index][0]}"
                # )

    def stop(self):
        """Stop updating polynomials in FPGA."""
        if self._no_poly_timer is not None:
            self._no_poly_timer.cancel()
            self._no_poly_timer = None
        for sub in self.delay_poly_subs.values():
            sub.on_new_poly_cbk(None)
        self.fpga.enable_vd = False


def valid_epoch(epoch: typing.Optional[int]) -> bool:
    """Test if epoch from delay source is valid."""
    if epoch is None or epoch == 0:
        return False
    return True
